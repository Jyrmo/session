<?php

namespace Jyrmo\Session;

class Session
{
    // TODO: exceptions if key not set.

    public function set(string $key, $val)
    {
        $this->startIfNotStarted();
        $_SESSION[$key] = $val;
    }

    public function get(string $key)
    {
        $this->startIfNotStarted();

        return $_SESSION[$key];
    }

    public function unset(string $key)
    {
        $this->startIfNotStarted();
        unset($_SESSION[$key]);
    }

    public function isSet(string $key) : bool
    {
        $this->startIfNotStarted();
        $isSet = isset($_SESSION[$key]);

        return $isSet;
    }

    public function unsetAll()
    {
        $this->startIfNotStarted();
        session_unset();
    }

    public function destroy()
    {
        // TODO: throw exception if destroy returns false.

        $this->startIfNotStarted();
        session_destroy();
    }

    public function startIfNotStarted()
    {
        if (!$this->isStarted())
            session_start();
    }

    public function isStarted() : bool
    {
        $status = session_status();
        $isStarted = $status === PHP_SESSION_ACTIVE;

        return $isStarted;
    }
}
